---
# @var jitsi_version: stable # select which version of Jitsi to install. `stable` or `nightly`. The latest version will be installed.
jitsi_version: stable

# @var jitsi_meet_prosody_version: 0.11.* # The deb package version of prosody to install
jitsi_prosody_version: 0.11.*

# @var jitsi_last_n: 5 # How many video streams are transmitted by default.
jitsi_last_n: 5
# @example #
# set to `-1` to enable default behaviour (all video streams of all participants are shared with everybody. Puts a significant amount of load on the videobridges.
# ```yaml
# jitsi_last_n: -1
# ```
# @end

# @var jitsi_notice_message: '' # Message to show the users. Example: 'The service will be down for maintenance at 01:00 AM GMT, empty string to disable
jitsi_notice_message: ''

# @var jitsi_meet_videobridge_secret: 'jvb_secret'
# @var jitsi_meet_videobridge_password: 'jvb_password'
# @var jitsi_meet_jicofo_secret: 'jicofo_secret'
# @var jitsi_meet_jicofo_password: 'jicofo_pw'

# Default auth information, used in multiple service templates.
jitsi_meet_jicofo_user: focus
jitsi_meet_jicofo_port: 5347

# The Jitsi components use the standard Java log levels, see:
# https://docs.oracle.com/javase/7/docs/api/java/util/logging/Level.html
# When using log aggregation for jitsi-meet components, set to "WARNING".
jitsi_meet_jicofo_loglevel: INFO
# The default config file at /etc/jitsi/videobridge/config claims the default port
# for JVB is "5275", but the manual install guide references "5347".
# https://github.com/jitsi/jitsi-meet/blob/master/doc/manual-install.md
jitsi_meet_videobridge_port: 5347

jitsi_meet_videobridge_loglevel: ALL

# @var jitsi_meet_videobridge_ram_max_mb: 3072 # The maximum size of the JVB's heap size, in megabytes.
jitsi_meet_videobridge_ram_max_mb: 3072

# A recent privacy-friendly addition, see here for details:
# https://github.com/jitsi/jitsi-meet/issues/422
# https://github.com/jitsi/jitsi-meet/pull/427
jitsi_meet_disable_third_party_requests: true
# If set to true the 'Kick out' button will be disabled.
jitsi_meet_disable_kick: true
# If set to true all muting operations of remote participants will be disabled.
jitsi_meet_disable_remote_mute: false

# @var jitsi_meet_secure_domain_enabled: false # set to true to enable secure domain, you'll need to manage your own users
jitsi_meet_secure_domain_enabled: false

# @var jitsi_meet_secure_domain_users: [] # a list of dicts with the keys 'username' and 'password'.
jitsi_meet_secure_domain_users: []

jitsi_meet_apt_repos:
  stable:
    repo_url: 'deb https://download.jitsi.org/ stable/'
  unstable:
    repo_url: 'deb https://download.jitsi.org/ unstable/'

# @var jitsi_ssl_cert_path: /etc/ssl/{{ jitsi_fqdn }}.crt # the remote path to where your cert file is stored. You must place this file there yourself.
jitsi_ssl_cert_path: /etc/ssl/{{ jitsi_fqdn }}.crt
# @var jitsi_ssl_key_path: /etc/ssl/{{ jitsi_fqdn }}.key # the remote path to where your key file is stored. You must place this file there yourself.
jitsi_ssl_key_path: /etc/ssl/{{ jitsi_fqdn }}.key

# These debconf settings represent answers to interactive prompts during installation
# of the jitsi-meet deb package. If you use custom SSL certs, you may have to set more options.
jitsi_meet_debconf_settings:
  - name: jitsi-videobridge
    question: jitsi-videobridge/jvb-hostname
    value: "{{ jitsi_fqdn }}"
    vtype: string
  - name: jitsi-meet
    question: jitsi-meet/jvb-serve
    value: "false"
    vtype: boolean
  - name: jitsi-meet-prosody
    question: jitsi-meet-prosody/jvb-hostname
    value: "{{ jitsi_fqdn }}"
    vtype: string
  - name: jitsi-meet-web-config
    question: jitsi-meet/cert-choice
    value: "I want to use my own certificate"
    vtype: select
  - name: jitsi-meet-web-config
    question: jitsi-meet/cert-path-key
    value: "{{ jitsi_ssl_key_path }}"
    vtype: string
  - name: jitsi-meet-web-config
    question: jitsi-meet/cert-path-crt
    value: "{{ jitsi_ssl_cert_path }}"
    vtype: string

jitsi_packages:
  - jicofo
  - jitsi-meet-web
  - jitsi-meet-prosody
  - jitsi-meet-web-config

# jibri default settings
# @var jibri_enable: false # whether to enable jibri or not
jibri_enable: false
# @var jibri_recording: false # whether to enable recording with jibri
jibri_recording: false
# @var jibri_streaming: false # whether to enable streaming with jibri
jibri_streaming: false
# @var jibri_user_jibri_password: "jibriauthpass"
# @var jibri_user_recorder_password: "jibrirecorderpass"

# The unique id of the instance
jitsi_videobridge_unique_id: "{{ lookup('password', '/dev/null chars=ascii_letters,digits') | to_uuid }}"

# @var jitsi_brand_footer_items: see defaults/main.yml # A list of label/url pairs to display at the footer of the welcome page
jitsi_brand_footer_items:
  - label: Help
    url: https://guardianproject.info
  - label: Legal Notice
    url: https://guardianproject.info
# @example #
# ```yaml
# jitsi_brand_footer_items:
#   - label: Help
#     url: https://guardianproject.info
#   - label: Legal Notice
#     url: https://guardianproject.info
# ```
# @end
# @var jitsi_brand_name: Keanu Meet # the name of your jitsi meet instance
jitsi_brand_name: Keanu Meet
# @var jitsi_brand_org: The Guardian Project # The name of your organization
jitsi_brand_org: The Guardian Project
# @var jitsi_brand_left_watermark_url: https://gitlab.com/guardianproject/guardianprojectpublic/-/raw/master/Graphics/GuardianProject/pngs/logo-black-w256.png # a url to a transparent png that will be layered over every call and on the welcome page
jitsi_brand_left_watermark_url: https://gitlab.com/guardianproject/guardianprojectpublic/-/raw/master/Graphics/GuardianProject/pngs/logo-black-w256.png
# @var jitsi_brand_url: https://guardianproject.info # A link to your organization's site
jitsi_brand_url: https://guardianproject.info
# @var jitsi_brand_support_url: https://guardianproject.info # A link to your organizationan's support or contact page
jitsi_brand_support_url: https://guardianproject.info

# @var jitsi_octo_enabled: false # Whether to enable octo or not
jitsi_octo_enabled: false
# @var jitsi_private_ip: undefined # The private local ip octo and prometheus exporter should bind to
# @var jitsi_region: undefined # The region this instance is in
jitsi_octo_selection_strategy: RegionBasedBridgeSelectionStrategy
# @var jitsi_use_maxmind_free: true # set to false if you have a paid subscription to maxmind
jitsi_use_maxmind_free: true
# @var jitsi_octo_nginx_geoip_mapping_vars: "$geoip2_data_country_code:$geoip2_data_continent_name"
jitsi_octo_nginx_geoip_mapping_vars: "$geoip2_data_country_code:$geoip2_data_continent_name"
# @var jitsi_octo_nginx_regions_mapping: see defaults/main.yml # the region mapping
jitsi_octo_nginx_regions_mapping:
  - default us-east-2
  - ~^.*:EU eu-central-1
  - ~^.*:AF eu-central-1
  - ~^.*:NA us-east-2
  - ~^.*:SA us-east-2
  - ~^.*:AN us-east-2
  - ~^.*:AS ap-south-1
  - ~^.*:OC ap-south-1
# @example #
# ```yaml
# jitsi_octo_nginx_regions_mapping:
#   - default us-east-2
#   - ~^.*:EU eu-central-1
#   - ~^.*:AF eu-central-1
#   - ~^.*:NA us-east-2
#   - ~^.*:SA us-east-2
#   - ~^.*:AN us-east-2
#   - ~^.*:AS ap-south-1
#   - ~^.*:OC ap-south-1
# ```
# @end

# @var jitsi_nginx_conf_path: /etc/nginx/conf.d # the path to where the nginx geoip config should be placed
jitsi_nginx_conf_path: /etc/nginx/conf.d
# @var jitsi_nginx_vhost_path: /etc/nginx/conf.d # the path to where the jitsi nginx vhost config should be placed
jitsi_nginx_vhost_path: /etc/nginx/conf.d


jitsi_exporter_scrape_url: http://localhost:8080/colibri/stats
jitsi_exporter_version: 0.2.6
jitsi_exporter_checksum: sha256:12d761a2f1e6d0b02fdc30d32fd6ca8b5f5bc02259228b192d9b6a76287ca4e3

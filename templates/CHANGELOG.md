# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.6](https://gitlab.com/guardianproject-ops/ansible-jitsi/compare/0.1.5...0.1.6) (2021-05-31)
